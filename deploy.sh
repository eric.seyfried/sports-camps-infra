#!/bin/sh


if [ -d "/var/docker/sports-camps-infra" ]
then
        cd /var/docker/sports-camps-infra
        sudo git pull && echo "sports-camps-infra pulled"
        # stop and remove containers
        sudo docker stop webserver db sports-camp-app && docker rm webserver db sports-camp-app
else
        sudo mkdir /var/docker
        cd /var/docker
        sudo git clone https://gitlab.com/eric.seyfried/sports-camps-infra.git && echo "sports-camps-infra cloned"
        cd /var/docker/sports-camps-infra
fi

# bring up containers
sudo docker-compose build --no-cache && \
        sudo chown -R 1001:1001 /var/docker/laravel && \
        sudo docker-compose -f docker-compose.prod.yml -f docker-compose.yml up -d && \
        echo "containers are up!!"

sleep 3
echo "CHECK FOR RANDOM GENERATED DB PASSWORD"
sudo docker logs db 2>/dev/null | grep "GENERATED ROOT PASSWORD" > db.pwd