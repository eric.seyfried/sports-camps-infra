app : GITEXISTS=$(shell [ -d $(shell pwd)/../laravel/.git ] && echo 1 || echo 0 )
LARAVEL_DIR=$(shell pwd)/../laravel/

docker:
	@docker-compose down && \
        docker-compose build --pull --no-cache && \
        docker-compose \
            -f docker-compose.dev.yml \
            -f docker-compose.yml \
        up -d --remove-orphans

app:
	@if [ 1 = $(GITEXISTS) ]; then\
		echo "git pull";\
		git --git-dir=$(LARAVEL_DIR)/.git pull && \
			docker run --rm -v $(LARAVEL_DIR)/:/app composer update;\
	else\
		git clone https://gitlab.com/eric.seyfried/sports-camps.git $(LARAVEL_DIR) && \
			cp $(LARAVEL_DIR)/.env.example $(LARAVEL_DIR)/.env && \
			docker run --rm -v $(LARAVEL_DIR)/:/app composer update && \
			docker exec sports-camp-app php artisan key:generate && \
			docker exec sports-camp-app php artisan config:cache && \
			docker exec sports-camp-app php artisan migrate;\
	fi

dev: docker app

restart:
	@docker-compose down && \
        docker-compose \
            -f docker-compose.dev.yml \
            -f docker-compose.yml \
        up -d --remove-orphans

debug:
	@git --git-dir=$(LARAVEL_DIR)/.git branch