## About Sports Camp Docker

This project contains all the docker infrastructure to run the Sports Camp application. It is currently hosted in [Digitial Ocean](https://www.digitalocean.com). The infrastructure design is a standard LEMP 3-tier architecture. 

Three containers are created:

- Webserver Container - nginx on ubuntu
- Application Container - php7-fpm with Laravel
- Database Container - mysql

## TL;DR;

### Getting started with a local dev environment

> Prerequisite: you must have docker installed locally

1. run `mkdir sports-camp && cd sports-camp`
1. git clone https://gitlab.com/eric.seyfried/sports-camps-infra.git
1. run `make dev`
1. run `docker ps` and you should see the three containers running.

## CI/CD

A GitLab pipeline is configured to deploy changes committed to `master` out to production (hosted on Digitalocean).

## Installation Notes

- The docker host is a 2 gb RAM /25 GB disk / Ubuntu Docker 5:19.03.1~3 on 18.04 droplet
- Create a deployer user with uid 1000 and with sudo 
- Create an ssh key for deployer user and put in GitLab CI/CD
- Create a www user with uid 100q and put in www group
- **IMPORTANT** run `docker logs db 2>/dev/null | grep "GENERATED ROOT PASSWORD" after initial docker up to get the mysql password. Put this in the GitLab variable in CI/CD
- The following variables need to be in GitLab CI/CD:
    - DB_HOST (db)
    - DB_PASSWORD (the random generated one)
    - DB_USERNAME (root)
    - SSH_PRIVATE_KEY (deployer user ssh key)
- Generate a Deploy Key for Digital Ocean Host and place in the Deploy Keys section in CI/CD